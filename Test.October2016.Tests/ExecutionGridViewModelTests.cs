﻿using System.Threading;
using NUnit.Framework;
using Test.October2016.Model;
using Test.October2016.ViewModel;
using Test.October2016.ViewModel.Services;

namespace Test.October2016.Tests
{
    [TestFixture]
    public class ExecutionGridViewModelTests
    {
        private const int UpdateTime = 3000;

        /// <summary>
        /// Test collection for updating only after updateTime elapsed
        /// </summary>
        [Test]
        public void TestCollectionUpdate()
        {
            var executionGridViewModel = new ExecutionGridViewModel();
            var progressDialogViewModel = new ProgressDialogViewModel(new ExecutionGenerator(), new ExecutionUpdater(UpdateTime));        
            progressDialogViewModel.StartAsyncCommand.Execute(null);
            Assert.AreEqual(executionGridViewModel.Executions.Count, 0);
            var result = progressDialogViewModel.StartAsyncCommand.Execution.Task.Wait(UpdateTime);
            // task succefully finished at the certain time 
            Assert.AreEqual(result, true);
            // wait for finish another background task
            Thread.Sleep(UpdateTime);
            Assert.AreNotEqual(executionGridViewModel.Executions.Count, 0);
        }

        /// <summary>
        /// Test for limit our collection
        /// </summary>
        [Test]
        public void TestLimitCollection()
        {
            const uint maxItemCount = 10;
            var executionGridViewModel = new ExecutionGridViewModel(maxItemCount);
            var progressDialogViewModel = new ProgressDialogViewModel(new ExecutionGenerator(), new ExecutionUpdater(UpdateTime));

            bool result = progressDialogViewModel.StartAsyncCommand.Execution.Task.Wait(UpdateTime / 2 + 1000);
            
            Assert.AreEqual(result, true);
            Assert.AreEqual(executionGridViewModel.Executions.Count, maxItemCount);
        }
    }
}