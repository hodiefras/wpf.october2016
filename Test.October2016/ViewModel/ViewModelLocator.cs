/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Test.October2016"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using Autofac;

namespace Test.October2016.ViewModel
{
    /// <summary>
    ///     This class contains static references to all the view models in the
    ///     application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        private static Bootstrapper _bootstrapper;

        /// <summary>
        ///     Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            if (_bootstrapper == null)
                _bootstrapper = new Bootstrapper();
        }


        public ProgressDialogViewModel ProgressDialogViewModel
            => _bootstrapper.Container.Resolve<ProgressDialogViewModel>();

        public ExecutionGridViewModel ExecutionGridViewModel
            => _bootstrapper.Container.Resolve<ExecutionGridViewModel>();

        public MainViewModel Main => _bootstrapper.Container.Resolve<MainViewModel>();

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}