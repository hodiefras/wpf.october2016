﻿using Autofac;
using Test.October2016.Model;
using Test.October2016.ViewModel.Services;

namespace Test.October2016.ViewModel
{
    /// <summary>
    ///     Class for configure creation and wrapper for a IoC-part of logic
    /// </summary>
    public class Bootstrapper
    {
        private const int UpdateTime = 3000;
        private const int MaxCountRepresentItems = 1000;

        public Bootstrapper()
        {
            ConfigureContainer();
        }

        public IContainer Container { get; set; }

        /// <summary>
        ///     Create the stucture of main classes
        /// </summary>
        private void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ExecutionUpdater>().WithParameter(new TypedParameter(typeof (int), UpdateTime));
            builder.RegisterType<ExecutionGenerator>();
            builder.RegisterType<ExecutionGridViewModel>()
                .WithParameter(new TypedParameter(typeof (uint), MaxCountRepresentItems));
            builder.RegisterType<ProgressDialogViewModel>();
            builder.RegisterType<MainViewModel>();

            Container = builder.Build();
        }
    }
}