using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Test.October2016.Model;
using Test.October2016.ViewModel.Infrastructure;
using Test.October2016.ViewModel.Services;

namespace Test.October2016.ViewModel
{
    public class ProgressDialogViewModel : ViewModelBase
    {
        #region Private fields

        private readonly ExecutionGenerator _executionGenerator;
        private ExecutionUpdater _executionUpdater;
        private bool _isIndeterminate;

        #endregion

        #region Constructor and public properties

        public ProgressDialogViewModel(ExecutionGenerator executionGenerator, ExecutionUpdater executionUpdater)
        {
            _executionGenerator = executionGenerator;
            _executionUpdater = executionUpdater;

            StartAsyncCommand = new AsyncCommand(o => Start());
            StopCommand = new RelayCommand(Stop);
            Messenger.Default.Register<NotificationMessage>(this, GenerateFinished);
        }

        public bool IsIndeterminate
        {
            get { return _isIndeterminate; }
            set
            {
                _isIndeterminate = value;
                RaisePropertyChanged();
            }
        }

        public AsyncCommand StartAsyncCommand { get; private set; }
        public ICommand StopCommand { get; private set; }

        #endregion

        #region Private handler methods

        /// <summary>
        ///     Run async background operation
        /// </summary>
        /// <returns></returns>
        private async Task Start()
        {
            if (IsIndeterminate)
                return;
            IsIndeterminate = true;
            await _executionGenerator.GenerateItemList();
        }

        private void Stop()
        {
            if (!IsIndeterminate)
                return;
            IsIndeterminate = false;
            _executionGenerator.TokenSource.Cancel();
        }

        private void GenerateFinished(NotificationMessage message)
        {
            if (message.Notification != "Finished") return;
            IsIndeterminate = false;
        }

        #endregion
    }
}