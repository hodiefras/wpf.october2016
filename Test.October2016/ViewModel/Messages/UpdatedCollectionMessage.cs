using GalaSoft.MvvmLight.Messaging;

namespace Test.October2016.ViewModel.Messages
{
    /// <summary>
    ///     Updated message with VM of Execution
    /// </summary>
    public class UpdatedCollectionMessage : MessageBase
    {
        public UpdatedCollectionMessage(ExecutionViewModel updatedExecution)
        {
            UpdatedExecution = updatedExecution;
        }

        public ExecutionViewModel UpdatedExecution { get; private set; }
    }
}