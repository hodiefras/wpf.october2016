using GalaSoft.MvvmLight.Messaging;
using Test.October2016.Model;

namespace Test.October2016.ViewModel.Messages
{
    /// <summary>
    ///     Generated message from generator in model
    /// </summary>
    public class GeneratedCollectionMessage : MessageBase
    {
        public GeneratedCollectionMessage(IExecution generatedExecution)
        {
            GeneratedExecution = generatedExecution;
        }

        public IExecution GeneratedExecution { get; private set; }
    }
}