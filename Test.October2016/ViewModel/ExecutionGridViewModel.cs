﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Test.October2016.ViewModel.Messages;

namespace Test.October2016.ViewModel
{
    public class ExecutionGridViewModel : ViewModelBase
    {
        #region Handlers

        private void UpdateExpiredSource(object sender, EventArgs e)
        {
            foreach (var execution in Executions.Where(ex => ex.IsNeedUpdateIsExpired))
            {
                execution.RaisePropertyChanged(nameof(execution.IsExpired));
            }
        }

        #endregion

        #region Private fields

        private readonly TimeSpan _updateTimePeriod = new TimeSpan(0, 0, 5);
        private readonly uint _maxItemCount;
        private BindingList<ExecutionViewModel> _executions;

        #endregion

        #region Constuctor and public properties

        /// <summary>
        ///     Initialize main items
        /// </summary>
        /// <param name="maxItemCount">maximum number of items, 0 means unlimit</param>
        public ExecutionGridViewModel(uint maxItemCount = 0)
        {
            _maxItemCount = maxItemCount;
            Executions = new BindingList<ExecutionViewModel>();

            Messenger.Default.Register<UpdatedCollectionMessage>(this, e => AddExecution(e.UpdatedExecution));
            var dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += UpdateExpiredSource;
            dispatcherTimer.Interval = _updateTimePeriod;
            dispatcherTimer.Start();
        }

        private void AddExecution(ExecutionViewModel execution)
        {
            // TODO may be change to array-type init in constuctor
            if (_maxItemCount > 0 && _maxItemCount <= Executions.Count)
                return;
            Executions.Add(execution);
        }

        /// <summary>
        ///     Main list of items for represent
        /// </summary>
        public BindingList<ExecutionViewModel> Executions
        {
            get { return _executions; }
            set
            {
                _executions = value;
                RaisePropertyChanged();
            }
        }

        #endregion
    }
}