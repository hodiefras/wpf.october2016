﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Test.October2016.ViewModel.Infrustructure;

namespace Test.October2016.ViewModel.Infrastructure
{
    public class AsyncCommand : AsyncCommand<object>
    {
        public AsyncCommand(Func<object, Task> command)
            : base(async (parmater, token) =>
            {
                await command(parmater);
                return null;
            }, null)
        {
        }

        public AsyncCommand(Func<object, Task> command, Func<object, bool> canExecute)
            : base(async (parmater, token) =>
            {
                await command(parmater);
                return null;
            }, canExecute)
        {
        }

        public AsyncCommand(Func<object, CancellationToken, Task> command)
            : base(async (parmater, token) =>
            {
                await command(parmater, token);
                return null;
            }, null)
        {
        }

        public AsyncCommand(Func<object, CancellationToken, Task> command, Func<object, bool> canExecute)
            : base(async (parmater, token) =>
            {
                await command(parmater, token);
                return null;
            }, canExecute)
        {
        }
    }

    public class AsyncCommand<TResult> : AsyncCommandBase, INotifyPropertyChanged
    {
        private readonly CancelAsyncCommand _cancelCommand;
        private readonly Func<object, bool> _canExecute;
        private readonly Func<object, CancellationToken, Task<TResult>> _command;
        private bool _allowMultipleInvocations;
        private NotifyTaskCompletion<TResult> _execution;

        public AsyncCommand(Func<object, Task<TResult>> command)
            : this((parmater, token) => command(parmater), null)
        {
        }

        public AsyncCommand(Func<object, Task<TResult>> command, Func<object, bool> canExecute)
            : this((parmater, token) => command(parmater), canExecute)
        {
        }

        public AsyncCommand(Func<object, CancellationToken, Task<TResult>> command)
            : this(command, null)
        {
        }

        public AsyncCommand(Func<object, CancellationToken, Task<TResult>> command, Func<object, bool> canExecute)
        {
            _command = command;
            _canExecute = canExecute;
            _cancelCommand = new CancelAsyncCommand();
        }

        public bool AllowMultipleInvocations
        {
            get { return _allowMultipleInvocations; }
            set
            {
                if (_allowMultipleInvocations == value)
                    return;

                _allowMultipleInvocations = value;
                OnPropertyChanged();
            }
        }

        public ICommand CancelCommand
        {
            get { return _cancelCommand; }
        }

        public NotifyTaskCompletion<TResult> Execution
        {
            get { return _execution; }
            private set
            {
                _execution = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public override bool CanExecute(object parameter)
        {
            var canExecute = _canExecute == null || _canExecute(parameter);
            var executionComplete = Execution == null || Execution.IsCompleted;

            return canExecute && (AllowMultipleInvocations || executionComplete);
        }

        public override async Task ExecuteAsync(object parameter)
        {
            _cancelCommand.NotifyCommandStarting();
            Execution = new NotifyTaskCompletion<TResult>(_command(parameter, _cancelCommand.Token));
            RaiseCanExecuteChanged();
            await Execution.TaskCompletion;
            _cancelCommand.NotifyCommandFinished();
            RaiseCanExecuteChanged();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private sealed class CancelAsyncCommand : ICommand
        {
            private bool _commandExecuting;
            private CancellationTokenSource _cts = new CancellationTokenSource();

            public CancellationToken Token
            {
                get { return _cts.Token; }
            }

            bool ICommand.CanExecute(object parameter)
            {
                return _commandExecuting && !_cts.IsCancellationRequested;
            }

            void ICommand.Execute(object parameter)
            {
                _cts.Cancel();
                RaiseCanExecuteChanged();
            }

            public event EventHandler CanExecuteChanged
            {
                add { CommandManager.RequerySuggested += value; }
                remove { CommandManager.RequerySuggested -= value; }
            }

            public void NotifyCommandStarting()
            {
                _commandExecuting = true;
                if (!_cts.IsCancellationRequested)
                    return;
                _cts = new CancellationTokenSource();
                RaiseCanExecuteChanged();
            }

            public void NotifyCommandFinished()
            {
                _commandExecuting = false;
                RaiseCanExecuteChanged();
            }

            private void RaiseCanExecuteChanged()
            {
                CommandManager.InvalidateRequerySuggested();
            }
        }
    }
}