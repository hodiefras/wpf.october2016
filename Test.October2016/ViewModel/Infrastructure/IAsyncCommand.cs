using System.Threading.Tasks;
using System.Windows.Input;

namespace Test.October2016.ViewModel.Infrastructure
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
    }
}