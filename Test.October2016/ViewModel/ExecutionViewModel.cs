﻿using System;
using GalaSoft.MvvmLight;
using Test.October2016.Model;

namespace Test.October2016.ViewModel
{
    /// <summary>
    ///     VM for Execution
    /// </summary>
    public class ExecutionViewModel : ViewModelBase
    {
        /// <summary>
        ///     An external property in comparision with model class
        /// </summary>
        private readonly TimeSpan _expiredTime = new TimeSpan(0, 0, 10);


        private bool _isExpired;

        public ExecutionViewModel(IExecution execution)
        {
            ExecutionReference = execution.ExecutionReference;
            AsOf = execution.AsOf;
            Size = execution.Size;
            Price = execution.Price;
            OrderReference = execution.OrderReference;
            AddedTime = DateTime.Now;
        }

        public DateTime AddedTime { get; }

        /// <summary>
        ///     Property for improvement update process
        /// </summary>
        public bool IsNeedUpdateIsExpired
        {
            get
            {
                var oldValue = _isExpired;
                var newValue = DateTime.Now - AddedTime >= _expiredTime;
                return oldValue != newValue;
            }
        }

        public bool IsExpired
        {
            get
            {
                var newValue = DateTime.Now - AddedTime >= _expiredTime;
                if (_isExpired == newValue) return _isExpired;
                _isExpired = newValue;
                return _isExpired;
            }
        }

        public string ExecutionReference { get; protected set; }
        public long OrderReference { get; protected set; }
        public decimal Size { get; set; }
        public decimal Quantity => Math.Abs(Size);
        public ExecutionAction Action => Size >= 0 ? ExecutionAction.Bought : ExecutionAction.Sold;
        public decimal Price { get; set; }
        public DateTimeOffset AsOf { get; set; }
    }
}