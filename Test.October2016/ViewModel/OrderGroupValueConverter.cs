using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Xceed.Wpf.DataGrid;

namespace Test.October2016.ViewModel
{
    /// <summary>
    ///     Converter for group title
    /// </summary>
    public class OrderGroupValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var group = value as Group;
            if (group == null)
                return string.Empty;
            // TODO add size???
            var size = @group.GetItems().OfType<ExecutionViewModel>().Sum(execution => execution.Size);

            var average =
                @group.GetItems()
                    .OfType<ExecutionViewModel>()
                    .Select(execution => execution.Price/2)
                    .Sum();
            var result = $"Size: {size} Average price: {average}";
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Empty;
        }
    }
}