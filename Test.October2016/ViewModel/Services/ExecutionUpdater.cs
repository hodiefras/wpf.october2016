using System.Collections.Generic;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Test.October2016.Model;
using Test.October2016.ViewModel.Messages;

namespace Test.October2016.ViewModel.Services
{
    /// <summary>
    ///     For update vm
    /// </summary>
    public class ExecutionUpdater
    {
        // in milliseconds
        private readonly int _updatedTimePeriod;

        // Queue of updated item
        private readonly Queue<IExecution> UpdateQueue;

        public ExecutionUpdater(int updateTimePeriod)
        {
            UpdateQueue = new Queue<IExecution>();
            _updatedTimePeriod = updateTimePeriod;
            Messenger.Default.Register<GeneratedCollectionMessage>(this,
                e => { UpdateQueue.Enqueue(e.GeneratedExecution); });
            var task = DoUpdateAsync();
        }

        /// <summary>
        ///     Update VM with certain time period
        /// </summary>
        /// <returns></returns>
        private async Task DoUpdateAsync()
        {
            while (true)
            {
                while (UpdateQueue.Count > 0)
                {
                    var execution = UpdateQueue.Dequeue();
                    // TODO optimize for sending a batch of items
                    Messenger.Default.Send(new UpdatedCollectionMessage(new ExecutionViewModel(execution)));
                }

                await Task.Delay(_updatedTimePeriod);
            }
        }
    }
}