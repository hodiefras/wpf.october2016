﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;

namespace Test.October2016.View
{
    /// <summary>
    ///     Логика взаимодействия для MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {
        public MainWindowView()
        {
            InitializeComponent();
            Messenger.Default.Register<NotificationMessage>(this, NotificationMessageReceived);
        }

        /// <summary>
        ///     Create new view after pressing generate button
        /// </summary>
        /// <param name="msg"></param>
        private void NotificationMessageReceived(NotificationMessage msg)
        {
            if (msg.Notification != "ShowGenerate") return;
            // TODO add verification for create new window
            var view = new ProgressDialogView {Owner = this};
            view.Show();
        }
    }
}