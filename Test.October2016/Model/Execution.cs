﻿using System;

namespace Test.October2016.Model
{
    /// <summary>
    ///     Execution
    /// </summary>
    public class Execution : IExecution
    {
        public Execution(string executionReference, long orderReference, DateTimeOffset asOf, decimal size,
            decimal price)
        {
            ExecutionReference = executionReference;
            AsOf = asOf;
            Size = size;
            Price = price;
            OrderReference = orderReference;
        }


        public string ExecutionReference { get; protected set; }
        public long OrderReference { get; protected set; }
        public decimal Size { get; protected set; }
        public decimal Quantity => Math.Abs(Size);
        public ExecutionAction Action => Size >= 0 ? ExecutionAction.Bought : ExecutionAction.Sold;
        public decimal Price { get; protected set; }
        public DateTimeOffset AsOf { get; protected set; }
    }
}