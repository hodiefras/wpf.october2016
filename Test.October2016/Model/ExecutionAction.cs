namespace Test.October2016.Model
{
    /// <summary>
    ///     ExecutionAction
    /// </summary>
    public enum ExecutionAction
    {
        Bought,
        Sold
    }
}