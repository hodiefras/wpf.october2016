﻿using System;

namespace Test.October2016.Model
{
    /// <summary>
    ///     IExecution
    /// </summary>
    public interface IExecution
    {
        string ExecutionReference { get; }
        long OrderReference { get; }

        decimal Size { get; }
        decimal Quantity { get; }
        ExecutionAction Action { get; }
        decimal Price { get; }
        DateTimeOffset AsOf { get; }
    }
}