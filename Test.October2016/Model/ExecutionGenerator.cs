﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Test.October2016.ViewModel.Messages;

namespace Test.October2016.Model
{
    public class ExecutionGenerator
    {
        private const uint MaxNumberExecutionItems = 10000;

        public ExecutionGenerator()
        {
            TokenSource = new CancellationTokenSource();
        }

        /// <summary>
        ///     A cancellation token source for the background operations.
        /// </summary>
        public CancellationTokenSource TokenSource { get; set; }

        /// <summary>
        ///     Generate async methods
        /// </summary>
        /// <returns></returns>
        public async Task GenerateItemList()
        {
            var random = new Random();
            await Task.Run(() =>
            {
                for (var i = 0; i < MaxNumberExecutionItems; i++)
                {
                    var executionReference = "Execution " + random.Next(20000);
                    long orderReference = random.Next(10);
                    decimal size = random.Next(int.MaxValue);
                    decimal price = random.Next(100);
                    var generatedItem = new Execution(executionReference, orderReference, DateTimeOffset.MaxValue, size,
                        price);
                    Messenger.Default.Send(new GeneratedCollectionMessage(generatedItem));
                    if (TokenSource.IsCancellationRequested)
                    {
                        return;
                    }
                }
            });
        }
    }
}